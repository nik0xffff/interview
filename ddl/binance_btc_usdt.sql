create table binance_btc_usdt
(
    unix        numeric,
    symbol      text,
    open        numeric,
    high        numeric,
    low         numeric,
    close       numeric,
    volume_btc  numeric,
    volume_usdt numeric,
    tradecount  integer,
    trade_date  date
);

alter table binance_btc_usdt
    owner to postgres;

