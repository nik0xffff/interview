create view v_sol_ma
            (unix, symbol, open, high, low, close, volume_btc, volume_usdt, tradecount, trade_date, ma_200, ma_50,
             lagging_ma_50, lagging_ma_200)
as
SELECT ma_lagging.unix,
       ma_lagging.symbol,
       ma_lagging.open,
       ma_lagging.high,
       ma_lagging.low,
       ma_lagging.close,
       ma_lagging.volume_btc,
       ma_lagging.volume_usdt,
       ma_lagging.tradecount,
       ma_lagging.trade_date,
       ma_lagging.ma_200,
       ma_lagging.ma_50,
       ma_lagging.lagging_ma_50,
       ma_lagging.lagging_ma_200
FROM (SELECT moving_averages.unix,
             moving_averages.symbol,
             moving_averages.open,
             moving_averages.high,
             moving_averages.low,
             moving_averages.close,
             moving_averages.volume_btc,
             moving_averages.volume_usdt,
             moving_averages.tradecount,
             moving_averages.trade_date,
             moving_averages.ma_200,
             moving_averages.ma_50,
             lag(moving_averages.ma_50) OVER ()  AS lagging_ma_50,
             lag(moving_averages.ma_200) OVER () AS lagging_ma_200
      FROM (SELECT bbtc.unix,
                   bbtc.symbol,
                   bbtc.open,
                   bbtc.high,
                   bbtc.low,
                   bbtc.close,
                   bbtc.volume_btc,
                   bbtc.volume_usdt,
                   bbtc.tradecount,
                   bbtc.trade_date,
                   round(avg((bbtc.open + bbtc.high + bbtc.low + bbtc.close) / 4::numeric)
                         OVER (ORDER BY bbtc.trade_date ROWS BETWEEN 199 PRECEDING AND CURRENT ROW), 199) AS ma_200,
                   round(avg((bbtc.open + bbtc.high + bbtc.low + bbtc.close) / 4::numeric)
                         OVER (ORDER BY bbtc.trade_date ROWS BETWEEN 49 PRECEDING AND CURRENT ROW), 49)   AS ma_50
            FROM binance_btc_usdt bbtc
            ORDER BY bbtc.trade_date) moving_averages) ma_lagging;

alter table v_sol_ma
    owner to postgres;

