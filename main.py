import matplotlib.pyplot as plt
import pandas as pds
import matplotlib.dates as mpl_dates
from mplfinance.original_flavor import candlestick_ohlc
from sqlalchemy import create_engine


def run():
    # Create an engine instance

    alchemyEngine = create_engine('postgresql+psycopg2://user:pwd@localhost:5432/postgres')

    # Connect to PostgreSQL server

    dbConnection = alchemyEngine.connect()

    # Read data from PostgreSQL database table and load into a DataFrame instance

    dataFrame = pds.read_sql("select * from v_sol_ma", dbConnection)

    pds.set_option('display.expand_frame_repr', False)

    # ohlc = dataFrame.loc[:, ['date', 'open', 'high', 'low', 'close']]
    ohlc = dataFrame.loc[:, ['trade_date', 'open', 'high', 'low', 'close', 'ma_50', 'ma_200']]

    ohlc['trade_date'] = pds.to_datetime(ohlc['trade_date'])

    ohlc['trade_date'] = ohlc['trade_date'].apply(mpl_dates.date2num)

    ohlc = ohlc.astype(float)

    plt.style.use('ggplot')
    # Creating Subplots
    fig, ax = plt.subplots()

    candlestick_ohlc(ax, ohlc.values, width=0.6, colorup='green', colordown='red', alpha=0.8)

    # Setting labels & titles
    ax.set_xlabel('Date')
    ax.set_ylabel('Price')
    fig.suptitle('Daily Candlestick Chart - Binance BTC-USD')

    # Formatting Date
    date_format = mpl_dates.DateFormatter('%d-%m-%Y')
    ax.xaxis.set_major_formatter(date_format)

    ax.plot(ohlc['trade_date'], ohlc['ma_50'])
    ax.plot(ohlc['trade_date'], ohlc['ma_200'])
    fig.autofmt_xdate()

    fig.tight_layout()

    plt.show()

    dbConnection.close()


if __name__ == '__main__':
    run()
